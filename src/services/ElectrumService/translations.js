const translations = {
  serverNotFound: {
    bn: "সার্ভার পাওয়া যায়নি",
    da: "Kan ikke finde serveren",
    de: "Server nicht gefunden",
    el: "Δεν βρέθηκε διακομιστής",
    en: "Server not found",
    es: "Servidor no encontrado",
    fr: "Serveur non trouvé",
    hi: "सर्वर नहीं मिला",
    it: "Server non trovato",
    ja: "サーバーが見つかりません",
    jv: "Server ora ketemu",
    ko: "서버를 찾을 수 없습니다",
    mr: "सर्व्हर सापडला नाही",
    ms: "Server tidak dijumpai",
    nb: "Finner ikke server",
    pa: "ਸਰਵਰ ਨਹੀਂ ਮਿਲਿਆ",
    pt: "Servidor não encontrado",
    ru: "Сервер не найден",
    sv: "Kan inte hitta servern",
    ta: "சேவையகம் கிடைக்கவில்லை",
    te: "సర్వర్ కనుగొనబడలేదు",
    tr: "Sunucu bulunamadı",
    ur: "سرور نہیں ملا",
    vi: "Không tìm thấy máy chủ",
    zh: "找不到服务器",
  },
  revertServer: {
    bn: "প্রমাণিত সার্ভারে ফিরে যাচ্ছি",
    da: "Vender tilbage til kendt server",
    de: "Zurücksetzen auf bekannten Server",
    el: "Επιστροφή σε γνωστό διακομιστή",
    en: "Reverting to known server",
    es: "Volviendo al servidor conocido",
    fr: "Retour au serveur connu",
    hi: "ज्ञात सर्वर पर वापस",
    it: "Tornando al server noto",
    ja: "既知のサーバーに戻る",
    jv: "Balik menyang server sing dikenal",
    ko: "알려진 서버로 복구",
    mr: "ओळखलेल्या सर्व्हरवर परत जात आहे",
    ms: "Kembali ke pelayan yang diketahui",
    nb: "Går tilbake til kjent server",
    pa: "ਜਾਣਕਾਰੀ ਸਰਵਰ ਉੱਤੇ ਵਾਪਸ ਜਾ ਰਿਹਾ ਹੈ",
    pt: "Voltando para o servidor conhecido",
    ru: "Возврат к известному серверу",
    sv: "Återgår till känd server",
    ta: "அறியப்பட்ட சேவையகத்திற்கு மாறுபட்டு வருகிறது",
    te: "గొప్ప సర్వర్‌కు వెళ్ళుకుంటున్నాం",
    tr: "Bilinen sunucuya geri dönme",
    ur: "معروف سرور کو واپس لوٹ رہا ہے",
    vi: "Quay lại máy chủ đã biết",
    zh: "恢复到已知服务器",
  },
};
export default translations;
