const translations = {
  walletSettings: {
    ar: "إعدادات المحفظة",
    bn: "ওয়ালেট সেটিংস",
    da: "Indstillinger for Tegnebog",
    de: "Wallet-Einstellungen",
    el: "Ρυθμίσεις πορτοφολιού",
    en: "Wallet Settings",
    es: "Configuración de la billetera",
    fa: "تنظیمات کیف پول",
    fil: "Mga Setting ng Wallet",
    fr: "Paramètres du portefeuille",
    ha: "Saitunan Wallet",
    hi: "बटुए की सेटिंग्स",
    id: "Pengaturan Dompet",
    it: "Impostazioni del portafoglio",
    ja: "ウォレット設定",
    jv: "Pengaturan Wallet",
    ko: "지갑 설정",
    mr: "वॉलेट सेटिंग्ज",
    ms: "Tetapan Wallet",
    nb: "Innstillinger for Lommebok",
    nl: "Wallet-instellingen",
    pa: "ਵਾਲੇਟ ਸੈਟਿੰਗਜ਼",
    pl: "Ustawienia portfela",
    pt: "Configurações da carteira",
    ro: "Setări portofel",
    ru: "Настройки кошелька",
    sv: "Inställningar för Plånbok",
    sw: "Mipangilio ya Wallet",
    ta: "பணப்பை அமைப்புகள்",
    te: "వాలెట్ సెట్టింగ్స్",
    th: "การตั้งค่ากระเป๋าเงิน",
    tr: "Cüzdan Ayarları",
    uk: "Налаштування гаманця",
    ur: "والیٹ سیٹنگز",
    vi: "Cài đặt Ví tiền",
    zh: "钱包设置",
    zh_TW: "錢包設置",
  },
  advancedOptions: {
    ar: "خيارات متقدمة",
    bn: "উন্নত বিকল্পসমূহ",
    da: "Avancerede Valgmuligheder",
    de: "Erweiterte Optionen",
    el: "Προηγμένες επιλογές",
    en: "Advanced Options",
    es: "Opciones avanzadas",
    fa: "گزینه های پیشرفته",
    fil: "Mga Advanced na Opsyon",
    fr: "Options avancées",
    ha: "Zaɓuɓɓuka na ci gaba",
    hi: "एडवांस्ड सेटिंग्स",
    id: "Opsi Lanjutan",
    it: "Opzioni avanzate",
    ja: "高度なオプション",
    jv: "Pilihan Lanjutan",
    ko: "고급 옵션",
    mr: "उन्नत पर्याय",
    ms: "Pilihan Lanjutan",
    nb: "Avanserte Alternativer",
    nl: "Geavanceerde mogelijkheden",
    pa: "ਤਰਕੀਬੀ ਚੋਣਾਂ",
    pl: "Zaawansowane opcje",
    pt: "Opções avançadas",
    ro: "Opțiuni avansate",
    ru: "Расширенные настройки",
    sv: "Avancerade Alternativ",
    sw: "Chaguzi za Juu",
    ta: "மேம்படுத்தப்பட்ட விருப்பங்கள்",
    te: "అధ్యయనం ఎంపికలు",
    th: "ตัวเลือกขั้นสูง",
    tr: "Gelişmiş Seçenekler",
    uk: "Розширені опції",
    ur: "اعلی تر کارروائیں",
    vi: "Tùy chọn nâng cao",
    zh: "高级选项",
    zh_TW: "高級選項",
  },
  additionalWalletInformation: {
    bn: "অতিরিক্ত ওয়ালেট তথ্য",
    da: "Mere Information Om Tegnebog",
    de: "Zusätzliche Wallet-Informationen",
    el: "Επιπρόσθετες πληροφορίες πορτοφολιού",
    en: "Additional Wallet Information",
    es: "Información adicional de la billetera",
    fr: "Informations supplémentaires sur le portefeuille",
    hi: "अतिरिक्त वॉलेट सूचना",
    it: "Informazioni aggiuntive sul portafoglio",
    ja: "追加のウォレット情報",
    jv: "Informasi Wallet Tambahan",
    ko: "추가 지갑 정보",
    mr: "अतिरिक्त वॉलेट माहिती",
    ms: "Maklumat Bantuan Dompet",
    nb: "Mer Informasjon Om Lommebok",
    pa: "ਵਾਲੇਟ ਵਿੱਚ ਵਾਧੂ ਜਾਣਕਾਰੀ",
    pt: "Informações adicionais da carteira",
    ru: "Дополнительная информация о кошельке",
    sv: "Mer Information Om Plånbok",
    ta: "கூடுதல் பணப் பற்றி தகவல்",
    te: "అదనపు బ్యాంక్ సమాచారం",
    tr: "Ekstra Cüzdan Bilgisi",
    ur: "اضافی والیٹ معلومات",
    vi: "Thông tin ví tiền bổ sung",
    zh: "额外钱包信息",
  },
  created: {
    ar: "مخلوق",
    bn: "তৈরি করা হয়েছে",
    da: "Oprettet",
    de: "Erstellt",
    el: "Δημιουργήθηκε",
    en: "Created",
    es: "Creado",
    fa: "ایجاد شده",
    fil: "Nilikha",
    fr: "Créé",
    ha: "Ƙirƙiri",
    hi: "बनाया गया",
    id: "Dibuat",
    it: "Creato",
    ja: "作成済み",
    jv: "Dijupuk",
    ko: "생성됨",
    mr: "तयार केले आहे",
    ms: "Dicipta",
    nb: "Opprettet",
    nl: "Gemaakt",
    pa: "ਬਣਾਇਆ ਗਿਆ",
    pl: "Utworzony",
    pt: "Criado",
    ro: "Creată",
    ru: "Создано",
    sv: "Skapad",
    sw: "Imeundwa",
    ta: "உருவாக்கப்பட்டது",
    te: "సృష్టించబడింది",
    th: "สร้าง",
    tr: "Oluşturuldu",
    uk: "Створено",
    ur: "تخلیق کیا گیا",
    vi: "Đã tạo",
    zh: "已创建",
    zh_TW: "已創建",
  },
  lastKnownBalance: {
    ar: "آخر رصيد معروف",
    bn: "সর্বশেষ পরিচিত ব্যালেন্স",
    da: "Sidst Kendte Saldo",
    de: "Letzter bekannter Kontostand",
    el: "Τελευταίο γνωστό υπόλοιπο",
    en: "Last Known Balance",
    es: "Último saldo conocido",
    fa: "آخرین موجودی شناخته شده",
    fil: "Huling Kilalang Balanse",
    fr: "Dernier solde connu",
    ha: "Ma&#39;auni Na Ƙarshe na Ƙarshe",
    hi: "सबसे नया बैलेंस",
    id: "Saldo Terakhir Diketahui",
    it: "Ultimo saldo conosciuto",
    ja: "最終既知の残高",
    jv: "Saldo Paling Anyar Ngrungokake",
    ko: "마지막으로 알려진 잔액",
    mr: "अखेरीस ओळखलेले शिल्लक",
    ms: "Baki Terakhir Diketahui",
    nb: "Siste Kjente Saldo",
    nl: "Laatst bekende saldo",
    pa: "ਆਖਰੀ ਜਾਣਕਾਰੀ ਸ਼ੇਸ਼",
    pl: "Ostatnie znane saldo",
    pt: "Último saldo conhecido",
    ro: "Ultimul sold cunoscut",
    ru: "Последний известный баланс",
    sv: "Senast Kända Saldo",
    sw: "Salio Linalojulikana Mwisho",
    ta: "கடைசி தெரியும் இருப்பு",
    te: "చివరి తెలియాబడిన శిల్లుక",
    th: "ยอดคงเหลือล่าสุดที่ทราบ",
    tr: "Son Bilinen Bakiye",
    uk: "Останній відомий баланс",
    ur: "آخری معروف بقیہ",
    vi: "Số dư cuối cùng đã biết",
    zh: "最后已知余额",
    zh_TW: "最後已知餘額",
  },
  walletActive: {
    ar: "المحفظة نشطة",
    bn: "ওয়ালেট সক্রিয়",
    da: "Tegnebog Aktiv",
    de: "Aktive Brieftasche",
    el: "Ενεργό πορτοφόλι",
    en: "Wallet Active",
    es: "Billetera activa",
    fa: "کیف پول فعال",
    fil: "Aktibo ang Wallet",
    fr: "Portefeuille actif",
    ha: "Wallet Active",
    hi: "चालू बटुआ",
    id: "Dompet Aktif",
    it: "Portafoglio attivo",
    ja: "ウォレットアクティブ",
    jv: "Wallet Active",
    ko: "지갑 활성화",
    mr: "वॉलेट सक्रिय",
    ms: "Wallet Aktif",
    nb: "Lommebok Aktiv",
    nl: "Portemonnee Actief",
    pa: "ਵਾਲੇਟ ਐਕਟਿਵ",
    pl: "Portfel aktywny",
    pt: "Carteira Ativa",
    ro: "Portofel activ",
    ru: "Активный кошелек",
    sv: "Plånbok Aktiv",
    sw: "Wallet Inatumika",
    ta: "பணப் போதுமானது",
    te: "వాలెట్ ఆక్టివ్",
    th: "Wallet ใช้งานอยู่",
    tr: "Etkin Cüzdan",
    uk: "Гаманець активний",
    ur: "والٹ سرگرم",
    vi: "Ví hoạt động",
    zh: "钱包已激活",
    zh_TW: "錢包活躍",
  },
  activateWallet: {
    ar: "تنشيط المحفظة",
    bn: "ওয়ালেট সক্রিয় করুন",
    da: "Aktiver Tegnebog",
    de: "Wallet aktivieren",
    el: "Ενεργοποίηση πορτοφολιού",
    en: "Activate Wallet",
    es: "Activar billetera",
    fa: "کیف پول را فعال کنید",
    fil: "I-activate ang Wallet",
    fr: "Activer le portefeuille",
    ha: "Kunna Wallet",
    hi: "बटुआ चालू करे",
    id: "Aktifkan Dompet",
    it: "Attiva il portafoglio",
    ja: "ウォレットをアクティベートする",
    jv: "Mangkat Wallet",
    ko: "지갑 활성화",
    mr: "वॉलेट सक्रिय करा",
    ms: "Aktifkan Wallet",
    nb: "Aktiver Lommebok",
    nl: "Activeer Portemonnee",
    pa: "ਵਾਲੇਟ ਚਾਲੂ ਕਰੋ",
    pl: "Aktywuj Portfel",
    pt: "Ativar carteira",
    ro: "Activați portofelul",
    ru: "Активировать кошелек",
    sv: "Aktivera Plånbok",
    sw: "Washa Wallet",
    ta: "பணப் போதுமாக்குங்கள்",
    te: "వాలెట్ సక్రియం చేయండి",
    th: "เปิดใช้งานกระเป๋าเงิน",
    tr: "Cüzdanı Etkinleştir",
    uk: "Активуйте Wallet",
    ur: "والٹ چالو کریں",
    vi: "Kích hoạt ví",
    zh: "激活钱包",
    zh_TW: "激活錢包",
  },
  deleteWallet: {
    ar: "حذف المحفظة",
    bn: "ওয়ালেট মুছুন",
    da: "Slet Tegnebog",
    de: "Wallet löschen",
    el: "Διαγραφή πορτοφολιού",
    en: "Delete Wallet",
    es: "Eliminar billetera",
    fa: "کیف پول را حذف کنید",
    fil: "Tanggalin ang Wallet",
    fr: "Supprimer le portefeuille",
    ha: "Share Wallet",
    hi: "बटुआ डिलीट कर दो",
    id: "Hapus Dompet",
    it: "Elimina il portafoglio",
    ja: "ウォレットを削除する",
    jv: "Hapus Wallet",
    ko: "지갑 삭제",
    mr: "वॉलेट डिलीट करा",
    ms: "Padam Wallet",
    nb: "Slett Lommebok",
    nl: "Portemonnee verwijderen",
    pa: "ਵਾਲੇਟ ਹਟਾਓ",
    pl: "Usuń Portfel",
    pt: "Excluir carteira",
    ro: "Ștergeți portofelul",
    ru: "Удалить кошелек",
    sv: "Radera Plånbok",
    sw: "Futa Wallet",
    ta: "பணப் போதுமாக்குங்கள்",
    te: "వాలెట్ తొలగించండి",
    th: "ลบกระเป๋าเงิน",
    tr: "Cüzdanı Sil",
    uk: "Видалити гаманець",
    ur: "والٹ حذف کریں",
    vi: "Xóa ví",
    zh: "删除钱包",
    zh_TW: "刪除錢包",
  },
  areYouSure: {
    ar: "هل أنت متأكد؟ أموالك في خطر",
    bn: "আপনি কি নিশ্চিত? আপনার টাকা ঝুঁকিতে আছে",
    da: "ER DU SIKKER? DINE PENGE ER I FARE",
    de: "Bist du sicher? Dein Geld ist in Gefahr",
    el: "Είστε σίγουρος/η; Τα χρήματά σας βρίσκονται σε κίνδυνο",
    en: "ARE YOU SURE? YOUR MONEY IS AT RISK",
    es: "¿ESTÁS SEGURO? TU DINERO ESTÁ EN RIESGO",
    fa: "مطمئنی؟ پول شما در معرض خطر است",
    fil: "SIGURADO KA BA? ANG IYONG PERA AY NANGANIB",
    fr: "ÊTES-VOUS SÛR ? VOTRE ARGENT EST EN DANGER",
    ha: "KA TABBATA? KUDIN KA ANA HATSARI",
    hi: "क्या आप निश्चित हैं? आपका पैसा गायब हो सकता है",
    id: "APA KAMU YAKIN? UANG ANDA BERISIKO",
    it: "SEI SICURO? I TUOI SOLDI SONO A RISCHIO",
    ja: "本当によろしいですか？お金が危険にさらされています",
    jv: "APAKAH SAMPEY YAKIN? DUWITEMU RISIKO",
    ko: "정말 확실합니까? 당신의 돈이 위험합니다",
    mr: "खरंतर आपलं खरं आहे का? आपले पैसे धोक्यात आहेत",
    ms: "ANDA PASTI? WANG ANDA BERISIKO",
    nb: "ER DU SIKKER? PENGENE DINE ER I FARE",
    nl: "WEET JE HET ZEKER? UW GELD IS IN RISICO",
    pa: "ਕੀ ਤੁਸੀਂ ਯਕੀਨੀ ਹੋ? ਤੁਹਾਡਾ ਪੈਸਾ ਖ਼ਤਰੇ ਵਿੱਚ ਹੈ",
    pl: "JESTEŚ PEWNY? TWOJE PIENIĄDZE SĄ ZAGROŻONE",
    pt: "VOCÊ TEM CERTEZA? SEU DINHEIRO ESTÁ EM RISCO",
    ro: "ESTI SIGUR? BANII TĂI SUNT ÎN PERICOL",
    ru: "ВЫ УВЕРЕНЫ? ВАШИ ДЕНЬГИ НАХОДЯТСЯ В ОПАСНОСТИ",
    sv: "ÄR DU SÄKER? DINA PENGAR ÄR I FARA",
    sw: "UNA UHAKIKA? PESA YAKO INA HATARI",
    ta: "உங்களுக்கு உறுதியாக இருக்கிறீர்களா? உங்கள் பணம் ஆபத்தில் உள்ளது",
    te: "మీరు ఖచ్చితంగా ఉందా? మీ డబ్బు హతాశంగా ఉంది",
    th: "คุณแน่ใจไหม? เงินของคุณมีความเสี่ยง",
    tr: "EMİN MİSİNİZ? PARANIZ TEHLİKEDE",
    uk: "ТИ ВПЕВНЕНИЙ? ВАШІ ГРОШІ ПІД РИЗИКОМ",
    ur: "کیا آپ یقینی ہیں؟ آپکا پیسہ خطرے میں ہے",
    vi: "BẠN CHẮC CHẮN? TIỀN CỦA BẠN ĐANG GẶP RỦI RO",
    zh: "您确定吗？您的资金有风险",
    zh_TW: "你確定嗎？您的資金面臨風險",
  },
  ensureRecoveryPhrase: {
    ar: "تأكد من كتابة عبارة الاسترداد الخاصة بك",
    bn: "নিশ্চিত হউন যে আপনি আপনার পুনরুদ্ধার পদক্ষেপটি লিখেছেন",
    da: "SØRG FOR AT DU HAR SKREVET DIN GENDANNELSESSÆTNING NED",
    de: "STELLEN SIE SICHER, DASS SIE IHREN WIEDERHERSTELLUNGSSATZ GESCHRIEBEN HABEN",
    el: "ΒΕΒΑΙΩΘΕΙΤΕ ΌΤΙ ΈΧΕΤΕ ΓΡΆΨΕΙ ΤΗΝ ΑΠΟΚΑΤΑΣΤΑΣΗ ΣΑΣ ΦΡΆΣΗ",
    en: "MAKE SURE YOU HAVE WRITTEN YOUR RECOVERY PHRASE",
    es: "ASEGÚRESE DE HABER ESCRITO SU FRASE DE RECUPERACIÓN",
    fa: "مطمئن شوید که عبارت بازیابی خود را نوشته اید",
    fil: "Tiyaking NAISULAT MO ANG IYONG PARIRALA SA PAGBAWI",
    fr: "ASSUREZ-VOUS D'AVOIR ÉCRIT VOTRE PHRASE DE RÉCUPÉRATION",
    ha: "KADA KA RUBUTA MAGANAR FARUWA",
    hi: "सुनिश्चित करें कि आपने अपना रिकवरी मंत्र लिख लिया है",
    id: "PASTIKAN ANDA TELAH MENULIS FRASA PEMULIHAN ANDA",
    it: "ACCERTATI DI AVER SCRITTO LA TUA FRASE DI RECUPERO",
    ja: "リカバリーフレーズを書いたことを確認してください",
    jv: "Pastikan Sampeyan Uga Teges Tegese Sampun Ditulis",
    ko: "복구 구문을 작성했는지 확인하세요",
    mr: "नक्की करा की आपण आपलं पुनर्प्राप्ती शब्द लिहिलं आहे",
    ms: "PASTIKAN ANDA TELAH MENULIS FRASE PEMULIHAN ANDA",
    nb: "SØRG FOR AT DU HAR SKREVET NED GJENOPPRETTINGSFRASEN",
    nl: "ZORG ERVOOR DAT JE JE HERSTELZIN HEBT GESCHREVEN",
    pa: "ਯਕੀਨੀ ਬਣਾਉਣ ਲਈ ਜ਼ਰੂਰੀ ਹੈ ਕਿ ਤੁਸੀਂ ਆਪਣਾ ਰੀਕਵਰੀ ਜ਼ਬਾਨ ਲਿਖਿਆ ਹੈ",
    pl: "UPEWNIJ SIĘ, ŻE NAPISAŁEŚ SWOJĄ ZWROTĘ REZYGNACYJNĄ",
    pt: "TENHA CERTEZA DE QUE VOCÊ ESCREVEU SUA FRASE DE RECUPERAÇÃO",
    ro: "ASIGURAȚI-VĂ ȚI ȚI SCRIS FRAZA DE RECUPERARE",
    ru: "УБЕДИТЕСЬ, ЧТО ВЫ НАПИСАЛИ СВОЙ ВОССТАНОВИТЕЛЬНЫЙ ФРАЗ",
    sv: "SE TILL ATT DU HAR SKRIVIT NER DIN ÅTERSTÄLLNINGSFRAS",
    sw: "HAKIKISHA UMEANDIKA NENO LAKO LA KUPONA",
    ta: "உங்கள் மீட்பு வாசகவியலை எழுதியுள்ளீர்கள் என்று உறுதிப்படுத்துங்கள்",
    te: "మీ రికవరీ వాక్యం రాయడం నిర్ధారించుకోండి",
    th: "ตรวจสอบให้แน่ใจว่าคุณได้เขียนวลีการกู้คืนของคุณแล้ว",
    tr: "KURTARMA CÜMLENİZİ YAZDIĞINIZDAN EMİN OLUN",
    uk: "ПЕРЕКОНАЙТЕСЯ, ЩО ВИ НАПИСАЛИ ФРАЗУ ВІДНОВЛЕННЯ",
    ur: "یقینی بنائیں کہ آپ نے اپنا ریکوری فیز لکھ لیا ہے",
    vi: "HÃY CHẮC CHẮN RẰNG BẠN ĐÃ VIẾT CÂU KHÔI PHỤC CỦA MÌNH",
    zh: "请确保您已经写下了恢复短语",
    zh_TW: "確保您已寫下恢復短語",
  },
  confirmDelete: {
    ar: "نعم ، أريد الحذف",
    bn: "হ্যাঁ, আমি মুছে ফেলতে চাই",
    da: "Ja, jeg vil slette",
    de: "Ja, ich möchte löschen",
    el: "Ναι, θέλω να διαγράψω",
    en: "Yes, I want to delete",
    es: "Sí, quiero eliminar",
    fa: "بله میخوام حذف کنم",
    fil: "Oo, gusto kong tanggalin",
    fr: "Oui, je veux supprimer",
    ha: "Ee, ina so in goge",
    hi: "हाँ, मैं डिलीट करना चाहता हूँ",
    id: "Ya, saya ingin menghapus",
    it: "Sì, voglio eliminare",
    ja: "はい、削除します",
    jv: "Iya, aku pengin ngilangi",
    ko: "예, 삭제하고 싶어요",
    mr: "होय, माझं हटवा आहे",
    ms: "Ya, saya mahu memadam",
    nb: "Ja, jeg vil slette",
    nl: "Ja, ik wil verwijderen",
    pa: "ਜੀ ਹਾਂ, ਮੈਂ ਹਟਾਉਣਾ ਚਾਹਿਦਾ ਹਾਂ",
    pl: "Tak, chcę usunąć",
    pt: "Sim, quero deletar",
    ro: "Da, vreau să șterg",
    ru: "Да, я хочу удалить",
    sv: "Ja, jag vill radera",
    sw: "Ndiyo, nataka kufuta",
    ta: "ஆம், நான் நீக்க விரும்புகிறேன்",
    te: "అవును, నేను తొలగించాలనుకుంటున్నాను",
    th: "ใช่ ฉันต้องการลบ",
    tr: "Evet, silmek istiyorum",
    uk: "Так, я хочу видалити",
    ur: "جی ہاں ، میں ڈیلیٹ کرنا چاہتا ہوں",
    vi: "Có, tôi muốn xóa",
    zh: "是的，我想删除",
    zh_TW: "是的，我想刪除",
  },
  keepSecret: {
    ar: "احتفظ بهذه الرسالة السرية",
    bn: "এই বাক্যটি গোপন রাখুন",
    da: "BEHOLD DENNE SÆTNING HEMMELIG",
    de: "BEHALTEN SIE DIESEN SATZ GEHEIM",
    el: "ΔΙΑΤΗΡΗΣΤΕ ΑΥΤΗΝ ΤΗΝ ΦΡΑΣΗ ΜΥΣΤΙΚΗ",
    en: "KEEP THIS PHRASE SECRET",
    es: "MANTENGA ESTA FRASE EN SECRETO",
    fa: "این عبارت را مخفی نگه دارید",
    fil: "PANATILIHIN ANG PARIRALAANG ITO",
    fr: "GARDEZ CETTE PHRASE SECRÈTE",
    ha: "KIYAYE WANNAN MAGANAR SIRRIN",
    hi: "इस रिकवरी मंत्र को गुप्त रखें",
    id: "JAGA RAHASIA FRASA INI",
    it: "TENETE SEGRETA QUESTA FRASE",
    ja: "このフレーズを秘密に保つ",
    jv: "WETENG ILMU ING NGLEMURUPI",
    ko: "이 구문을 비밀로 유지하세요",
    mr: "हा वाक्य गोपन ठेवा",
    ms: "SIMPAN FRAZA INI SECREAT",
    nb: "HOLD DENNE FRASEN HEMMELIG",
    nl: "HOUD DEZE ZIN GEHEIM",
    pa: "ਇਹ ਵਾਕਾ ਰੱਖੋ ਗੁਪਤ",
    pl: "ZACHOWAJ TO ZDROWIE W TAJEMNICY",
    pt: "MANTENHA ESTA FRASE EM SEGREDO",
    ro: "PĂSTRAȚI SECRETA ACEASTA FRAZĂ",
    ru: "СОХРАНЯЙТЕ ЭТУ ФРАЗУ В СЕКРЕТЕ",
    sv: "HÅLL DEN HÄR FRASSEN HEMLIG",
    sw: "WEKA SIRI YA KIFUNGU HIKI",
    ta: "இந்த சொல் ரகசியமாகவே வைக்கவும்",
    te: "ఈ వాక్యాన్ని గుమ్మని ఉంచండి",
    th: "เก็บวลีนี้เป็นความลับ",
    tr: "BU CÜMLEYİ GİZLİ TUTUN",
    uk: "ЗБЕРІГАЙТЕ ЦЮ ФРАЗУ В ТАЄМНІЦІ",
    ur: "اس جملہ کو خفیہ رکھیں",
    vi: "GIỮ BÍ MẬT CÂU NÀY",
    zh: "保密此短语",
    zh_TW: "保守這句話的秘密",
  },
  dontStoreDigitally: {
    ar: "لا تخزن رقميا",
    bn: "ইলেক্ট্রনিক রূপে সংরক্ষণ করবেন না",
    da: "OPBEVAR IKKE DIGITALT",
    de: "NICHT DIGITAL SPEICHERN",
    el: "ΜΗΝ ΑΠΟΘΗΚΕΎΕΤΕ ΨΗΦΙΑΚΆ",
    en: "DO NOT STORE DIGITALLY",
    es: "NO GUARDE EN FORMATO DIGITAL",
    fa: "به صورت دیجیتالی ذخیره نکنید",
    fil: "HUWAG MAG-IMBAK NG DIGITAL",
    fr: "NE PAS STOCKER NUMÉRIQUEMENT",
    ha: "KAR KA KIMANIN DIGITALLY",
    hi: "इस रिकवरी मंत्र को डिजिटल रूप में न समेट कर रखे",
    id: "JANGAN MENYIMPAN DIGITAL",
    it: "NON ARCHIVIARE DIGITALMENTE",
    ja: "デジタルで保存しないでください",
    jv: "ORA SIMPAN DIGITAL",
    ko: "디지털로 저장하지 마세요",
    mr: "डिजिटलची संग्रह करू नका",
    ms: "JANGAN SIMPAN SECARA DIGITAL",
    nb: "IKKE OPPBEVAR DIGITALT",
    nl: "NIET DIGITAAL OPSLAAN",
    pa: "ਡਿਜਿਟਲ ਤੌਰ ਤੇ ਸੰਗ੍ਰਹ ਨਾ ਕਰੋ",
    pl: "NIE PRZECHOWYWAĆ W FORMIE CYFROWEJ",
    pt: "NÃO ARMAZENAR DIGITALMENTE",
    ro: "NU PĂSTRAȚI DIGITAL",
    ru: "НЕ ХРАНИТЬ В ЦИФРОВОМ ВИДЕ",
    sv: "FÖRVARA INTE DIGITALT",
    sw: "USIHIFADHI KWA DIGITALI",
    ta: "எண்களால் சேமிக்க வேண்டாம்",
    te: "డిజిటల్‌గా సేవ్ చేయవద్దు",
    th: "อย่าจัดเก็บแบบดิจิทัล",
    tr: "DİJİTAL OLARAK SAKLAMAYIN",
    uk: "НЕ ЗБЕРІГАЙТЕ В ЦИФРОВОМУ ВИРОБІ",
    ur: "ڈیجیٹل طور پر ذخیرہ نہ کریں",
    vi: "KHÔNG LƯU TRỮ SỐ",
    zh: "不要以数字形式存储",
    zh_TW: "不要以數字方式存儲",
  },
  viewRecoveryPhrase: {
    ar: "عرض عبارة استرداد المحفظة",
    bn: "ওয়ালেট পুনরুদ্ধার ফ্রেজ দেখুন",
    da: "Vis Gendannelsessætning For Tegnebog",
    de: "Wiederherstellungssatz der Geldbörse anzeigen",
    el: "Προβολή φράσης επαναφοράς πορτοφολιού",
    en: "View Wallet Recovery Phrase",
    es: "Ver frase de recuperación de la billetera",
    fa: "عبارت بازیابی کیف پول را مشاهده کنید",
    fil: "Tingnan ang Parirala sa Pagbawi ng Wallet",
    fr: "Afficher la phrase de récupération du portefeuille",
    ha: "Duba Jumlar farfadowa da Wallet",
    hi: "बटुए के रिकवरी मंत्र को खोलें",
    id: "Lihat Frasa Pemulihan Dompet",
    it: "Visualizza la frase di recupero del portafoglio",
    ja: "ウォレットのリカバリーフレーズを表示",
    jv: "Tingali frasa Recovery Wallet",
    ko: "지갑 복구 구문 보기",
    mr: "वॉलेट पुनर्प्राप्तीचा वाक्य बघा",
    ms: "Lihat Frasa Pemulihan Dompet",
    nb: "Vis Gjenopprettingsfrasen For Lommebok",
    nl: "Bekijk Wallet Recovery Phrase",
    pa: "ਵਾਲੇਟ ਰਿਕਵਰੀ ਫਰੇਜ਼ ਵੇਖੋ",
    pl: "Wyświetl frazę odzyskiwania portfela",
    pt: "Ver frase de recuperação da carteira",
    ro: "Vedeți expresia de recuperare a portofelului",
    ru: "Просмотреть фразу для восстановления кошелька",
    sv: "Visa Återställningsfras För Plånbok",
    sw: "Tazama Maneno ya Urejeshaji wa Wallet",
    ta: "பணப் படை மீட்டமைப்பு பார்க்க",
    te: "వాలెట్ మరుగుపు వాక్యాన్ని చూడండి",
    th: "ดูวลีการกู้คืน Wallet",
    tr: "Cüzdan Kurtarma Cümlesini Görüntüle",
    uk: "Переглянути фразу відновлення Wallet",
    ur: "والیٹ ریکوری فریز دیکھیں",
    vi: "Xem Cụm từ Khôi phục Ví",
    zh: "查看钱包恢复短语",
    zh_TW: "查看錢包恢復短語",
  },
  secretAndSecure: {
    ar: "تأكد من إبقائها سرية وآمنة!",
    bn: "এটি গোপন এবং নিরাপদে রাখতে নিশ্চিত হোন!",
    da: "Sørg for at holde den hemmelig og sikkert!",
    de: "Stellen Sie sicher, dass Sie es geheim und sicher aufbewahren!",
    el: "Βεβαιωθείτε ότι το κρατάτε μυστικό και ασφαλές!",
    en: "Make sure to keep it secret and secure!",
    es: "¡Asegúrate de mantenerlo en secreto y seguro!",
    fa: "مطمئن شوید که آن را مخفی و امن نگه دارید!",
    fil: "Siguraduhing panatilihin itong sikreto at secure!",
    fr: "Assurez-vous de le garder secret et en sécurité !",
    ha: "Tabbatar kiyaye shi sirri da tsaro!",
    hi: "इसे अत्यंत सुरक्षित और गुप्त रखें",
    id: "Pastikan untuk merahasiakannya dan aman!",
    it: "Assicurati di tenerlo segreto e al sicuro!",
    ja: "秘密に保ち、安全に保つようにしてください！",
    jv: "Pastikan tetep rahasia lan aman!",
    ko: "비밀로 유지하고 안전하게 보관하세요!",
    mr: "त्याचा गोपन आणि सुरक्षित ठेवण्यासाठी खात्री करा!",
    ms: "Pastikan ia dirahsiakan dan selamat!",
    nb: "Sørg for å holde den hemmelig og sikkert!",
    nl: "Zorg ervoor dat u het geheim en veilig houdt!",
    pa: "ਇਸਨੂੰ ਗੁਪਤ ਅਤੇ ਸੁਰੱਖਿਅਤ ਰੱਖਣ ਦੀ ਪੁਸ਼ਟੀ ਕਰੋ!",
    pl: "Pamiętaj, aby zachować to w tajemnicy i bezpiecznie!",
    pt: "Certifique-se de mantê-lo em segredo e seguro!",
    ro: "Asigurați-vă că îl păstrați secret și în siguranță!",
    ru: "Убедитесь, что оно остается в секрете и надежно!",
    sv: "Se till att hålla den hemmelig och säkert!",
    sw: "Hakikisha unaiweka kwa siri na salama!",
    ta: "இதை ரகசியமாக வைத்திருங்கள் மற்றும் பாதுகாக்க உறுதிப்படுத்துங்கள்!",
    te: "దానిని రహస్యంగా మరియు భద్రంగా ఉంచుకోండి!",
    th: "อย่าลืมเก็บเป็นความลับและปลอดภัย!",
    tr: "Onu gizli ve güvende tutmaktan emin olun!",
    uk: "Зберігайте це в таємниці та надійності!",
    ur: "یقینی بنائیں کہ یہ خفیہ اور محفوظ رکھیں!",
    vi: "Đảm bảo giữ nó bí mật và an toàn!",
    zh: "确保保密且安全！",
    zh_TW: "確保保密且安全！",
  },
  rebuildWallet: {
    ar: "إعادة إنشاء المحفظة",
    bn: "ওয়ালেট পুনর্নির্মাণ করুন",
    da: "Genopbyg Tegnebog",
    de: "Wallet neu erstellen",
    el: "Ανακατασκευή πορτοφολιού",
    en: "Rebuild Wallet",
    es: "Reconstruir Billetera",
    fa: "بازسازی کیف پول",
    fil: "Buuin muli ang Wallet",
    fr: "Reconstruire le portefeuille",
    ha: "Sake Gina Wallet",
    hi: "बटुये का पुनर्निर्माण करें",
    id: "Bangun kembali Dompet",
    it: "Ricostruisci il portafoglio",
    ja: "ウォレットを再構築する",
    jv: "Rebuild Wallet",
    ko: "지갑 재구성",
    mr: "वॉलेट पुनर्निर्मिती करा",
    ms: "Bina semula Wallet",
    nb: "Gjenoppbygg Lommebok",
    nl: "Portefeuille opnieuw opbouwen",
    pa: "ਵਾਲੇਟ ਦੁਬਾਰਾ ਬਣਾਓ",
    pl: "Odbuduj portfel",
    pt: "Reconstruir a carteira",
    ro: "Reconstruiți portofelul",
    ru: "Восстановить кошелек",
    sv: "Bygg Om Plånbok",
    sw: "Jenga Upya Wallet",
    ta: "பணப்பை மீட்டமைக்கவும்",
    te: "వాలెట్ రీబిల్డ్ చేయండి",
    th: "สร้างกระเป๋าเงินใหม่",
    tr: "Cüzdanı Yeniden Oluştur",
    uk: "Відновити гаманець",
    ur: "والیٹ دوبارہ تعمیر کریں",
    vi: "Xây dựng lại Ví tiền",
    zh: "重建钱包",
    zh_TW: "重建錢包",
  },
};

export default translations;
