import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { ImportOutlined } from "@ant-design/icons";
import * as bip39 from "bip39";
import Accordion from "@/components/atoms/Accordion";
import WalletManagerService from "@/services/WalletManagerService";
import { translate } from "@/util/translations";
import translations from "./translations";

import { DEFAULT_DERIVATION_PATH, DERIVATION_PATHS } from "@/util/crypto";

export default function SettingsWalletWizardImport() {
  const navigate = useNavigate();

  const [mnemonicInput, setMnemonicInput] = useState("");
  const [passphraseInput, setPassphraseInput] = useState("");
  const [message, setMessage] = useState("");
  const [derivationPath, setDerivationPath] = useState(DEFAULT_DERIVATION_PATH);

  const handleMnemonicInput = (event) => {
    const sanitizedInput = event.target.value
      .toLowerCase()
      .replace(/[^a-z ]/g, "") // strip all non a-z
      .replace(/ {2,}/g, " "); // strip consecutive spaces

    setMnemonicInput(sanitizedInput);
    setMessage("");
  };

  const handlePassphraseInput = (event) => {
    setPassphraseInput(event.target.value);
  };

  const handleDerivationSelect = (event) => {
    setDerivationPath(event.target.value);
  };

  const handleImportWallet = () => {
    const trimmedInput = mnemonicInput.trim();
    const wordCount = trimmedInput.split(" ").length;
    if (wordCount !== 12 && wordCount !== 24) {
      setMessage(translate(translations.exactWordCount));
      return;
    }

    const isValidMnemonic = bip39.validateMnemonic(trimmedInput);

    if (isValidMnemonic) {
      try {
        const wallet = WalletManagerService().importWallet(
          trimmedInput,
          passphraseInput,
          derivationPath
        );
        navigate(`/settings/wallet/${wallet.id}`, { replace: true });
      } catch (e) {
        setMessage(translate(translations.alreadyImported));
      }
    } else {
      setMessage(translate(translations.phraseInvalid));
    }
  };

  return (
    <>
      <div className="text-2xl text-center text-neutral-900">
        {translate(translations.enterRecoveryPhrase)}
      </div>
      <div className="flex justify-center">
        {message === "" ? (
          <ul className="list-disc list-inside p-2 text-left text-neutral-700">
            <li>{translate(translations.alsoKnownAs)}</li>
            <li>{translate(translations.exactly12Or24)}</li>
          </ul>
        ) : (
          <div className="text-error p-2">{message}</div>
        )}
      </div>
      <div className="rounded-md border-4 border-primary">
        <textarea
          className="w-full text-mono h-36 max-h-36 resize-none"
          onChange={handleMnemonicInput}
          value={mnemonicInput}
          autoComplete="off"
        />
      </div>
      <div className="my-1">
        <Accordion icon={() => null} title="Additional Options">
          <Accordion.Child icon={() => null} label="Passphrase">
            <input
              type="text"
              className="w-full border border-primary"
              onChange={handlePassphraseInput}
              value={passphraseInput}
              autoComplete="off"
            />
          </Accordion.Child>
          <Accordion.Child icon={() => null} label="Derivation Path">
            <select onChange={handleDerivationSelect} value={derivationPath}>
              {DERIVATION_PATHS.map((path) => (
                <option value={path}>{path}</option>
              ))}
            </select>
          </Accordion.Child>
        </Accordion>
      </div>
      <div className="my-2">
        <button
          type="button"
          className="bg-primary text-white w-full rounded-lg p-2"
          onClick={handleImportWallet}
        >
          <ImportOutlined className="text-2xl" />{" "}
          {translate(translations.importWallet)}
        </button>
      </div>
    </>
  );
}
