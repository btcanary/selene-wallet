// Time
export const ONE_SECOND = 1000; // in milliseconds
export const TEN_SECONDS = ONE_SECOND * 10; // in milliseconds
export const THIRTY_SECONDS = ONE_SECOND * 30; // in milliseconds
