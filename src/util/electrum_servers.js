export const electrum_servers = [
  "cashnode.bch.ninja", // Kallisti
  "fulcrum.jettscythe.xyz", // Jett
  "bch.imaginary.cash", // im_uname
  "bitcoincash.network", // Dagur
  "electroncash.dk",
  "bch.loping.net",
  "bch.soul-dev.com",
];

export const chipnet_servers = [
  "chipnet.bch.ninja", // Kallisti
  "chipnet.imaginary.cash", // im_uname
  "cbch.loping.net",
];
